module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    sass: {
    	dist: {
		    options: {
		      style: 'expanded' // cssmin will minify later
		    },
		    files: {
		      'css/build/svgs-for-post.css': 'css/scss/svgs-for-post.scss'
		    }
		  }
	  },

	  autoprefixer: {
	    options: {
		    browsers: ['last 2 version']
		  },
		  multiple_files: {
		    expand: true,
		    flatten: true,
		    src: 'css/build/*.css',
		    dest: 'css/build/prefixed/'
		  }
		},

		cssmin: {
			combine: {
		    files: {
		      'css/svgs-for-post.min.css': ['css/build/prefixed/svgs-for-post.css']
		    }
		  }
		},

    svgstore: {
		  options: {
		    prefix : 'shape-', // This will prefix each <g> ID
		    svg: { // will add and overide the the default xmlns="http://www.w3.org/2000/svg" attribute to the resulting SVG
			    style: "display:none;",
          xmlns: 'http://www.w3.org/2000/svg'
	      },
	      includedemo: false
		  },
		  default : {
	      files: {
	        'svgs-for-post.svg': ['svgs/*.svg']
	      }
	    }
		},

		watch: {
			svgs: {
		    files: 'svgs/*.svg',
		    tasks: ['svgstore'],
		    options: {
		      spawn: false,
		    }
		  },
		  css: {
		  	files: 'css/scss/*.scss',
		  	tasks: ['sass', 'autoprefixer', 'cssmin'],
		  	options: {
		  		spawn: false,
		  	}
		  }
		},

		connect: {
			server: {
		    options: {
		      port: 2000
		    }
		  }
		}

  });

  // Load the plugin that provides the "svgstore" task.
  grunt.loadNpmTasks('grunt-contrib-sass');
  grunt.loadNpmTasks('grunt-autoprefixer');
  grunt.loadNpmTasks('grunt-contrib-cssmin');
  grunt.loadNpmTasks('grunt-svgstore');
  grunt.loadNpmTasks('grunt-contrib-watch');
  grunt.loadNpmTasks('grunt-contrib-connect');

  // Default task(s).
  grunt.registerTask('default', ['connect', 'watch']);

};